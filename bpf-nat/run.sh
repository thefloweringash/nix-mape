#!/usr/bin/env bash

set -x
set -euo pipefail
rm -rf /tmp/vm-state-{isp,router,client} || true

nix-build -I nixpkgs=$HOME/src/nixpkgs test.nix -A driver
./result/bin/nixos-test-driver --interactive
