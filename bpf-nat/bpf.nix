{ stdenv, lib, llvmPackages, kernel, debug ? false }:

stdenv.mkDerivation {
  name = "map-e-bpf";

  src = lib.sourceFilesBySuffices ./. [ "Makefile" ".c" ".h"];

  buildInputs = with llvmPackages; [
    llvm clang kernel
  ];

  KDIR = "${lib.getDev kernel}/lib/modules/${kernel.modDirVersion}";

  ARCH = "x86"; # TODO: what is kernel.karch and why is it not x86? (it's x86_64)

  makeFlags = [ "--no-builtin-rules" ];

  EXTRA_CFLAGS = if debug then [ "-D DEBUG" ] else null;

  hardeningDisable = [ "all" ];
  dontStrip = true;

  installPhase = ''
    mkdir $out
    cp mape.o $out
  '';

  passthru = {
    testedKernelVersions = [ "5.15.43" "6.1.29" "6.6.4" "6.7.0" "6.8" "6.9.1" "6.10.7" "6.11" "6.12" "6.13"];
  };
}
