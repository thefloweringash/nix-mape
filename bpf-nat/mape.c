#define KBUILD_MODNAME "mape"

// This file includes fixes from BCC [1]. This ordering of preprocessor
// setup is delicate.
//
// [1] https://github.com/iovisor/bcc/blob/2d1497cde1cc9835f759a707b42dea83bee378b8/src/cc/export/helpers.h#L20-L41

#include <asm/types.h>

/* clang does not support "asm volatile goto" yet.
 * So redefine asm_volatile_goto to some invalid asm code.
 * If asm_volatile_goto is actually used by the bpf program,
 * a compilation error will appear.
 */
#ifdef asm_volatile_goto
#undef asm_volatile_goto
#endif
#define asm_volatile_goto(x...) asm volatile("invalid use of asm_volatile_goto")

#include <linux/compiler_types.h>

/* In Linux 5.4 asm_inline was introduced, but it's not supported by clang.
 * Redefine it to just asm to enable successful compilation.
 */
#ifdef asm_inline
#undef asm_inline
#define asm_inline asm
#endif

// End of ASM fixes

#include <asm/byteorder.h>

#include <linux/bpf.h>

#include <linux/filter.h>
#include <linux/in.h>
#include <linux/if_ether.h>
#include <linux/ip.h>
#include <linux/ipv6.h>
#include <linux/tcp.h>
#include <linux/icmp.h>


#ifdef __section
#undef __section
#endif
#define __section(x)  __attribute__((section(x), used))

#define likely(x) __builtin_expect(!!(x), 1)
#define unlikely(x) __builtin_expect(!!(x), 0)

#ifdef inline
#undef inline
#endif
#define inline __attribute__((always_inline))

#ifdef DEBUG
#define debug(x)                                \
    ({                                          \
        char __fmt[] = x;                       \
        bpf_trace_printk(__fmt, sizeof(__fmt)); \
    })

#define debugf(x, ...)                                          \
    ({                                                          \
        char __fmt[] = x;                                       \
        bpf_trace_printk(__fmt, sizeof(__fmt), __VA_ARGS__);    \
    })
#else
#define debug(x)
#define debugf(x, ...)
#endif

/* Some used BPF function calls. */
static int (*bpf_skb_store_bytes)(void *ctx, int off, void *from,
                                  int len, int flags) =
    (void *) BPF_FUNC_skb_store_bytes;

static int (*bpf_l4_csum_replace)(void *ctx, int off, int from,
                                  int to, int flags) =
    (void *) BPF_FUNC_l4_csum_replace;

static int (*bpf_trace_printk)(const char *fmt, int fmt_size, ...) =
    (void *) BPF_FUNC_trace_printk;


/* Some used BPF intrinsics. */
unsigned long long load_byte(void *skb, unsigned long long off)
    asm ("llvm.bpf.load.byte");

unsigned long long load_half(void *skb, unsigned long long off)
    asm ("llvm.bpf.load.half");

enum mangle_direction {
    DIR_INGRESS,
    DIR_EGRESS,
};

static inline __u16 swap_port(__u16 port, enum mangle_direction dir) {
    if (dir == DIR_EGRESS) {
        return (port & 0xf) | ((port & 0xff00) >> 4) | ((port & 0x00f0) << 8);
    }
    else {
        return (port & 0xf) | ((port & 0x0ff0) << 4) | ((port & 0xf000) >> 8);
    }
}

static inline void set_port(struct __sk_buff *skb, int nh_off,
                            int port_off, int check_off,
                            __u16 old, __u16 new)
{
    bpf_l4_csum_replace(skb, nh_off + check_off,
                        old, new, sizeof(new));

    bpf_skb_store_bytes(skb, nh_off + port_off,
                        &new, sizeof(new), 0);
}


static inline int mape_ipv4_header_len(struct __sk_buff *skb, int nh_off) {
    __u8 ip_vl = load_byte(skb, nh_off);
    if (likely(ip_vl == 0x45))
        return sizeof(struct iphdr);
    else
        return (ip_vl & 0xF) << 2;
}

static inline int mape_ipv4_is_fragment(struct __sk_buff *skb, int nh_off) {
    __u8 ip_frag_off = load_half(skb, nh_off + offsetof(struct iphdr, frag_off));

    return ip_frag_off & 0x1fff;
}

static inline int mape_ipv4_generic(
    struct __sk_buff *skb,
    int nh_off,
    int port_off,
    int check_off,
    enum mangle_direction dir
) {
    __u16 port = load_half(skb, nh_off + port_off);
    __u16 new_port = swap_port(port, dir);

    #ifdef DEBUG
    if (dir == DIR_EGRESS) {
        debugf("[egress] port mangled: %d -> %d\n", port, new_port);
    }
    else {
        debugf("[ingress] port demangled: %d -> %d\n", port, new_port);
    }
    #endif

    set_port(skb,
             nh_off,
             port_off,
             check_off,
             __cpu_to_be16(port),
             __cpu_to_be16(new_port));

    return -1;
}

static inline int mape_ipv4_icmp(struct __sk_buff *skb, int nh_off, enum mangle_direction dir) {
    __u8 type = load_byte(skb, nh_off + offsetof(struct icmphdr, type));
    __u8 code = load_byte(skb, nh_off + offsetof(struct icmphdr, code));

    debugf("icmp: %d:%d\n", type, code);

    if (type == ICMP_ECHO || type == ICMP_ECHOREPLY) {
        return mape_ipv4_generic(
            skb, nh_off,
            offsetof(struct icmphdr, un.echo.id),
            offsetof(struct icmphdr, checksum),
            dir
        );
    }

    nh_off += sizeof(struct icmphdr);

    // TODO: we check if the following looks like an IPv4 datagram,
    // then "recurse". Is this correct?
    //
    //  - are there icmp messages that include things other than
    //    datagram headers?

    __u8 ip_ver = load_byte(skb, nh_off);
    if (unlikely((ip_ver & 0xf0) != 0x40)) {
        debug("unhandled icmp body\n");
        return 0;
    }

    __u8 ip_proto = load_byte(skb, nh_off + offsetof(struct iphdr, protocol));
    if (unlikely(ip_proto != IPPROTO_TCP && ip_proto != IPPROTO_UDP && ip_proto != IPPROTO_ICMP)) {
        debugf("[icmp] ignoring unknown protocol %d\n", ip_proto);
        return 0;
    }

    if (unlikely(mape_ipv4_is_fragment(skb, nh_off))) {
        debug("[icmp] ignoring ip fragment\n");
        return 0;
    }

    nh_off += mape_ipv4_header_len(skb, nh_off);

    debug("icmp mangling nested ip header\n");

    int ret = 0;

    // This is slightly tricky: we're not demangling reply traffic,
    // but copies of errant packets. Consider an incoming "port
    // unreachable" packet. The copy includes our mangled source port,
    // which must be mapped according to dir. Contrast to a regular
    // reply, in which we'd be mapping the destination port.

    switch (ip_proto) {
    case IPPROTO_TCP: {
        int port_off = dir == DIR_INGRESS
            ? offsetof(struct tcphdr, source)
            : offsetof(struct tcphdr, dest);

        return mape_ipv4_generic(skb, nh_off, port_off, offsetof(struct tcphdr, check), dir);
    }

    case IPPROTO_UDP: {
        int port_off = dir == DIR_INGRESS
            ? offsetof(struct udphdr, source)
            : offsetof(struct udphdr, dest);

        return mape_ipv4_generic(skb, nh_off, port_off, offsetof(struct udphdr, check), dir);
    }

    case IPPROTO_ICMP: {
        type = load_byte(skb, nh_off + offsetof(struct icmphdr, type));

        if (type == ICMP_ECHO || type == ICMP_ECHOREPLY) {
            return mape_ipv4_generic(
                skb, nh_off,
                offsetof(struct icmphdr, un.echo.id),
                offsetof(struct icmphdr, checksum),
                dir
                                     );
        }
        return 0;
    }

    default:
        return 0; /* unreachable */
    }

}

static inline int mape_ipv4(struct __sk_buff *skb, int nh_off, enum mangle_direction dir) {
    __u8 ip_proto = load_byte(skb, nh_off + offsetof(struct iphdr, protocol));

    if (unlikely(ip_proto != IPPROTO_TCP && ip_proto != IPPROTO_UDP && ip_proto != IPPROTO_ICMP)) {
        debugf("ignoring unknown protocol %d\n", ip_proto);
        return 0;
    }

    if (unlikely(mape_ipv4_is_fragment(skb, nh_off))) {
        debug("ignoring ip fragment\n");
        return 0;
    }

    nh_off += mape_ipv4_header_len(skb, nh_off);

    switch (ip_proto) {
    case IPPROTO_TCP: {
        int port_off = dir == DIR_EGRESS
            ? offsetof(struct tcphdr, source)
            : offsetof(struct tcphdr, dest);

        return mape_ipv4_generic(skb, nh_off, port_off, offsetof(struct tcphdr, check), dir);
    }

    case IPPROTO_UDP: {
        int port_off = dir == DIR_EGRESS
            ? offsetof(struct udphdr, source)
            : offsetof(struct udphdr, dest);

        return mape_ipv4_generic(skb, nh_off, port_off, offsetof(struct udphdr, check), dir);
    }

    case IPPROTO_ICMP:
        return mape_ipv4_icmp(skb, nh_off, dir);

    default:
        return 0; /* unreachable */
    }
}

static inline void dump(struct __sk_buff *skb, int nh_off) {
    #ifdef DEBUG
    int off = 0;
    #define LOOP_BODY                                           \
        {                                                       \
            __u8 byte = load_byte(skb, nh_off + off);           \
            debugf("byte[%d] = %d / 0x%x\n", off, byte, byte);  \
            off++;                                              \
        }
    LOOP_BODY;
    LOOP_BODY;
    LOOP_BODY;
    LOOP_BODY;
    LOOP_BODY;
    LOOP_BODY;
    LOOP_BODY;
    LOOP_BODY;
    #undef LOOP_BODY
    #endif
}

// ingress packets are decapsulated ipv4 packets (observed in linux 5.15)
__section("ingress") int mape_ingress(struct __sk_buff *skb) {
    int ret = 0;

    debug("ingress\n");

    if (likely(skb->protocol == __constant_htons(ETH_P_IP))) {
        ret = mape_ipv4(skb, 0, DIR_INGRESS);
    }


    debugf("ingress returning %d\n", ret);

    return ret;
}

// egress packets are straight ipv4
__section("egress") int mape_egress(struct __sk_buff *skb) {
    int ret = 0;

    debug("egress\n");

    if (likely(skb->protocol == __constant_htons(ETH_P_IP))) {
        ret = mape_ipv4(skb, 0, DIR_EGRESS);
    }

    debugf("egress returning %d\n", ret);

    return ret;
}

char __license[] __section("license") = "GPL";
