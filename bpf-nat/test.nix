import <nixpkgs/nixos/tests/make-test-python.nix> ({ pkgs, lib, ... }:
let
  localTunnelEP = "fd80:cafe:d00d:1000::1";
  brAddr = "fd80:cafe:d00d::1";

  baseCfg = {
    imports = [ dumpModule ];
    environment.systemPackages = with pkgs; [ tcpdump wireshark-cli bind.dnsutils socat traceroute ];
    networking.interfaces = lib.mkForce {};
    networking.useDHCP = false;
    services.getty.autologinUser = "root";
    systemd.network.networks.qemu = {
      matchConfig.Name = "eth0";
      linkConfig.Unmanaged = "yes";
    };
  };

  dumpModule = { config, lib, pkgs, ... }:
    let cfg = config.dump; in
  {
    options = {
      dump.interfaces = lib.mkOption {
        type = lib.types.listOf lib.types.str;
        default = [];
      };
    };
    config = lib.mkIf (cfg.interfaces != []) {
      systemd.services = lib.listToAttrs (lib.flip map cfg.interfaces (ifname:
        lib.nameValuePair "dump-${ifname}" {
          wantedBy = [ "multi-user.target" ];
          after = [ "sys-subsystem-net-devices-${ifname}.device" ];
          script = ''
            tcpdump -i ${ifname} -U -w /tmp/xchg/${ifname}.pcap
            tcpdump -i ${ifname} -n -U | logger -t "dump-${ifname}"
          '';
          path = with pkgs; [ tcpdump ];
          serviceConfig.Type = "simple";
          serviceConfig.Restart = "always";
          unitConfig.StartLimitIntervalSec = 0;
        }
      ));
    };
  };
in
{
  name = "map-e";
  nodes = {
    client = { pkgs, nodes, ... }: {
      imports = [ baseCfg ];
      virtualisation.vlans = [ 1 ];
      dump.interfaces = [ "eth1" ];

      systemd.network = {
        enable = true;

        networks.internal = {
          matchConfig.Name = "eth1";
          networkConfig.Address = [ "10.88.0.2/24" ];
          networkConfig.Gateway = "10.88.0.1";
        };
      };
    };

    router = { config, pkgs, nodes, ... }:
    let
      update-map-e = pkgs.writeScriptBin "update-map-e" ''
        #!${pkgs.runtimeShell}
        set -euo pipefail
        tc filter del dev tun0 ingress
        tc filter add dev tun0 ingress bpf da obj /tmp/xchg/bpfprog/mape.o section ingress
        tc filter del dev tun0 egress
        tc filter add dev tun0 egress bpf da obj /tmp/xchg/bpfprog/mape.o section egress
      '';
      configure-map-e = pkgs.writeScriptBin "configure-map-e" ''
        #!${pkgs.runtimeShell}
        set -euo pipefail
        export PATH=${lib.makeBinPath [ pkgs.iproute2 ]}:$PATH
        tc qdisc add dev tun0 clsact
        tc filter add dev tun0 ingress bpf da obj ${bpfProg}/mape.o section ingress
        tc filter add dev tun0 egress bpf da obj ${bpfProg}/mape.o section egress
      '';
      trace-pipe = pkgs.writeScriptBin "tp" ''
        #!${pkgs.runtimeShell} -x
        cat /sys/kernel/debug/tracing/trace_pipe
      '';

      bpfProg = pkgs.callPackage ./bpf.nix {
        kernel = config.system.build.kernel;
        debug = true;
      };
    in
    {
      imports = [ baseCfg ];
      virtualisation.vlans = [ 2 1 ];
      dump.interfaces = [ "eth1" "eth2" ];
      boot.kernel.sysctl."net.core.bpf_jit_enable" = 1;


      networking.useDHCP = lib.mkForce true;
      networking.dhcpcd.enable = lib.mkForce true;
      networking.dhcpcd.extraConfig = ''
        noipv6rs
        denyinterfaces eth2
        interface eth1
          ipv6rs
          ia_pd 1 eth2/0
      '';

      systemd.network = {
        enable = true;

        networks.internal = {
          matchConfig.Name = "eth2";
          networkConfig.Address = [
            "10.88.0.1/24"
          ];
          networkConfig.IPForward = "yes";
        };

        networks.upstream = {
          matchConfig.Name = "eth1";
          networkConfig.Address = [ "fe80::1/64" ];
          networkConfig.IPForward = "ipv6";

          networkConfig.Tunnel = "tun0";
        };

        networks.tunnel = {
          matchConfig.Name = "tun0";
          networkConfig.Address = "10.0.0.2/30";
          networkConfig.LinkLocalAddressing = "no";
          networkConfig.IPForward = "ipv4";
          # networkConfig.DefaultRouteOnDevice = true;
          extraConfig = ''
            [Network]
            DefaultRouteOnDevice = true
          '';
        };

        netdevs.tun0 = {
          netdevConfig = {
            Name = "tun0";
            Kind = "ip6tnl";
          };

          tunnelConfig = {
            Local = localTunnelEP;
            Remote = brAddr;
            Mode = "ipip6";
          };
        };

      };

      systemd.services.configure-map-e = {
        serviceConfig.ExecStart = "${configure-map-e}/bin/configure-map-e";
        wantedBy = [ "sys-subsystem-net-devices-tun0.device" ];
        after = [ "sys-subsystem-net-devices-tun0.device" ];
      };

      systemd.services.configure-nat = {
        script = let ports = "50960-51199"; in ''
          iptables -t nat -p tcp -o tun0 -A POSTROUTING -j MASQUERADE --to-ports ${ports}
          iptables -t nat -p udp -o tun0 -A POSTROUTING -j MASQUERADE --to-ports ${ports}
          iptables -t nat -p icmp -o tun0 -A POSTROUTING -j MASQUERADE --to-ports ${ports}
        '';
        wantedBy = [ "sys-subsystem-net-devices-tun0.device" ];
        after = [ "sys-subsystem-net-devices-tun0.device" ];
        path = with pkgs; [ iptables ];
      };

      environment.systemPackages = [ configure-map-e update-map-e trace-pipe ];
      environment.variables."BPF_PROG" = "${bpfProg}/mape.o";

      systemd.services.systemd-networkd.environment."SYSTEMD_LOG_LEVEL" = "debug";
    };

    isp = { pkgs, nodes, ... }:
      let addPrefixScript = pkgs.writeScript "add-delegated-prefix" ''
          #!${pkgs.runtimeShell}
          set -x
          export PATH=${lib.makeBinPath [ pkgs.iproute2 ]}:$PATH
          ip -6 route add "$1" via fe80::1 dev eth1
        '';
      in
    {
      imports = [ baseCfg ];

      virtualisation.vlans = [ 2 ];

      networking.firewall.enable = false;

      dump.interfaces = [ "eth1" "tun0" ];

      services.nginx.enable = true;
      services.dnsmasq.enable = true;
      services.dnsmasq.extraConfig = ''
        port=54
      '';

      systemd.network = {
        enable = true;

        networks.internet = {
          matchConfig.Name = "eth1";
          networkConfig.Address = "fd80:cafe:d00d::1/64";
          networkConfig.Tunnel = "tun0";
        };

        networks.tunnel = {
          matchConfig.Name = "tun0";
          networkConfig.Address = "10.0.0.1/30";
        };

        netdevs.tun0 = {
          netdevConfig = {
            Name = "tun0";
            Kind = "ip6tnl";
          };

          tunnelConfig = {
            Local = brAddr;
            Remote = localTunnelEP;
            Mode = "ipip6";
          };
        };
      };

      services.radvd = {
        enable = true;
        config = ''
          interface eth1 {
            AdvSendAdvert on;
            AdvManagedFlag on;
            AdvOtherConfigFlag on;
          };
        '';
      };

      security.sudo.extraRules = [
        {
          commands = [ { command = toString addPrefixScript; options = [ "NOPASSWD" ]; } ];
          users = [ "dhcpd" ];
        }
      ];

      users.users.dhcpd = {
        isSystemUser = true;
        group = "dhcpd";
      };
      users.groups.dhcpd = {};
      systemd.services.dhcpd6.serviceConfig.DynamicUser = lib.mkForce "false";

      services.dhcpd6 = {
        enable = true;
        interfaces = [ "eth1" ];
        extraConfig = ''
          on commit {
            # https://lists.isc.org/pipermail/dhcp-users/2014-September/018210.html
            set iapd = binary-to-ascii(16,16,":", suffix(option dhcp6.ia-pd,16));
            set pdsize = binary-to-ascii(10,8,":",substring(suffix(option dhcp6.ia-pd,17),0,1));
            set pdnet = concat(iapd, "/", pdsize);
            execute("/run/wrappers/bin/sudo", "${addPrefixScript}", pdnet);
          }
          subnet6 fd80:cafe:d00d::/64 {
            prefix6 fd80:cafe:d00d:1000:: fd80:cafe:d00d:1000:: /56;
          }
        '';
      };
    };
  };

  testScript = { nodes, ... }: ''
    start_all()

    isp.wait_for_unit("nginx.service")

    client.wait_until_succeeds("ping -c 1 -W 1 10.0.0.1")
    client.succeed("curl --retry 10 --connect-timeout 10 http://10.0.0.1")
  '';
})
