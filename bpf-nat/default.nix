with (import <nixpkgs> {});

{
  bpfprog = callPackage ./bpf.nix { kernel = linux; debug = true; };
}
