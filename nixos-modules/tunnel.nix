{ config, lib, pkgs, ... }:

# BPF program attachment to a tunnel device

let
  inherit (lib) elemAt optionalAttrs mkOption types mkIf mkMerge flip
    concatMap range concatMapStringsSep mkEnableOption;

  inherit (import ./mape-lib.nix)
    mangleEgress demangleIngress;

  cfg = config.networking.mape.tunnel;

  bpfProg = pkgs.callPackage ../bpf-nat/bpf.nix {
    kernel = config.system.build.kernel;
    llvmPackages = pkgs.llvmPackages;
  };

  configure-map-e = pkgs.writeScriptBin "configure-map-e" ''
    #!${pkgs.runtimeShell}
    set -x
    set -euo pipefail
    export PATH=${lib.makeBinPath [ pkgs.iproute2 ]}:$PATH
    tc qdisc add dev ${cfg.interface} clsact
    tc filter add dev ${cfg.interface} ingress bpf da obj ${bpfProg}/mape.o section ingress
    tc filter add dev ${cfg.interface} egress bpf da obj ${bpfProg}/mape.o section egress
  '';

  teardown-map-e = pkgs.writeScriptBin "teardown-map-e" ''
    #!${pkgs.runtimeShell}
    set -x
    set -euo pipefail
    export PATH=${lib.makeBinPath [ pkgs.iproute2 ]}:$PATH
    tc filter del dev ${cfg.interface} ingress
    tc filter del dev ${cfg.interface} egress
    tc qdisc del dev ${cfg.interface} clsact
  '';

in

{
  options = {
    networking.mape.tunnel = {
      enable = mkEnableOption "mape tunnelling";

      checkKernelVersion = mkOption {
        type = types.bool;
        default = true;
        description = "Check kernel version against known good versions";
      };

      upstreamInterface = mkOption {
        type = types.str;
      };

      interface = mkOption {
        type = types.str;
        default = "tun0";
      };

      v4 = mkOption {
        type = types.str;
      };

      v6 = mkOption {
        type = types.str;
      };

      br = mkOption {
        type = types.str;
      };

      ports.low = mkOption {
        type = types.port;
        description = "Lowest available port (bottom of first port set)";
      };

      ports.high = mkOption {
        type = types.port;
        description = "Highest available port (top of last port set)";
      };
    };
  };

  config = mkIf cfg.enable {
    assertions = [
      (
        let
          current = lib.versions.majorMinor config.system.build.kernel.version;
          allTested = map lib.versions.majorMinor bpfProg.testedKernelVersions;
        in {
          assertion = cfg.checkKernelVersion -> lib.any (tested: current == tested) allTested;
          message = ''
            [nix-mape] Functionality has not been tested on this kernel series
              Tested versions: ${lib.concatStringsSep ", " bpfProg.testedKernelVersions}
              Current version: ${current}
          '';
        }
      )
    ];

    # set up the basic networking structure
    systemd.network = {
      # define the tunnel device
      netdevs."${cfg.interface}" = {
        netdevConfig = {
          Name = cfg.interface;
          Kind = "ip6tnl";
        };

        tunnelConfig = {
          Local = cfg.v6;
          Remote = cfg.br;
          Mode = "ipip6";
          EncapsulationLimit = "none";
        };
      };

      # configure addressing for the tunnel
      networks.tunnel = {
        matchConfig.Name = cfg.interface;
        networkConfig.Address = [ "${cfg.v4}/32" ];
        networkConfig.LinkLocalAddressing = "no";
        networkConfig.DefaultRouteOnDevice = true;

        routes = [
          { routeConfig = { Destination = "10.0.0.0/8"; Type = "unreachable"; }; }
          { routeConfig = { Destination = "172.16.0.0/12"; Type = "unreachable"; }; }
          { routeConfig = { Destination = "192.168.0.0/16"; Type = "unreachable"; }; }
        ];
      };

      # configure addressing for the upstream interface
      networks.upstream = {
        matchConfig.Name = cfg.upstreamInterface;
        networkConfig.Address = [ "${cfg.v6}/128" ];

        # and attach the tunnel to this interface
        networkConfig.Tunnel = cfg.interface;
      };
    };

    # Apply the first stage of nat, from internal to contiguous ports
    networking.mape.nftables = {
      tables.nat.family = "ip";

      tables.nat.chains.postrouting = {
        baseChain = {
          type = "nat";
          hook = "postrouting";
          priority = "srcnat";
        };

        rules = let
          ps = "${toString (demangleIngress cfg.ports.low)}-${toString (demangleIngress cfg.ports.high)}";
        in ''
          oif ${cfg.interface} ip protocol tcp  counter snat to ${cfg.v4}:${ps}
          oif ${cfg.interface} ip protocol udp  counter snat to ${cfg.v4}:${ps}
          oif ${cfg.interface} ip protocol icmp counter snat to ${cfg.v4}:${ps}
        '';
      };
    };

    # Apply the second stage of nat, from contiguous ports to split out port-sets
    boot.kernel.sysctl."net.core.bpf_jit_enable" = 1;
    systemd.services.configure-map-e = {
      serviceConfig = {
        ExecStart = "${configure-map-e}/bin/configure-map-e";
        ExecStop = "${teardown-map-e}/bin/teardown-map-e";
        Type = "oneshot";
        RemainAfterExit = true;
      };
      wantedBy = [ "sys-subsystem-net-devices-${cfg.interface}.device" ];
      after = [ "sys-subsystem-net-devices-${cfg.interface}.device" ];
    };

    # Delay firewall startup until tunnel device is created.  We can
    # then refer to the tunnel device by index and not name.
    systemd.services.nftables = {
      before = lib.mkForce [];
      wants = [ "sys-subsystem-net-devices-${cfg.interface}.device" ];
      after = [ "sys-subsystem-net-devices-${cfg.interface}.device" ];
    };
  };
}
