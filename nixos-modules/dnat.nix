{ config, lib, pkgs, ... }:

# MapE specific port forwarding, configuration is accepted in local ports and
# translated to bpf mangled ports.

let
  inherit (lib) elemAt optionalAttrs mkOption types mkIf mkMerge flip
    concatMap range concatMapStringsSep;

  inherit (import ./mape-lib.nix)
    mangleEgress demangleIngress;

  cfg = config.networking.mape;

  tunnelInterface = cfg.tunnel.interface;

  singlePortDest = {
    options = {
      host = mkOption { type = types.str; };
      port = mkOption { type = types.nullOr types.port; default = null; };
    };
  };

  portRangeDest = {
    options = {
      host = mkOption { type = types.str; };
      fromPort = mkOption { type = types.nullOr types.port; default = null; };
      toPort = mkOption { type = types.nullOr types.port; default = null; };
    };
  };


  portForwardType = {
    options = {
      to = mkOption {
        type = types.submodule singlePortDest;
      };

      port = mkOption {
        type = types.int;
      };

      protocol = mkOption {
        type = types.enum [ "udp" "tcp" ];
      };
    };
  };

  # TODO: assertion that a range is constrained to a single port set
  portRangeForwardType = {
    options = {
      to = mkOption {
        type = types.submodule portRangeDest;
      };

      portRange.from = mkOption {
        type = types.int;
      };

      portRange.to = mkOption {
        type = types.int;
      };

      protocol = mkOption {
        type = types.enum [ "udp" "tcp" ];
      };
    };
  };
in
{
  options = {
    networking.mape = {
      portForwards = mkOption {
        type = types.listOf (types.submodule portForwardType);
        default = [];
      };

      portRangeForwards = mkOption {
        type = types.listOf (types.submodule portRangeForwardType);
        default = [];
      };
    };
  };

  config = mkIf (cfg.portForwards != []) {
    networking.mape.nftables = {
      tables.nat.family = "ip";
      tables.nat.chains.prerouting = {
        baseChain = {
          type = "nat";
          hook = "prerouting";
        };
        rules = ''
          iif ${tunnelInterface} fib daddr type local ct state new counter jump mape-dnat
        '';
      };

      tables.nat.chains.mape-dnat = {
        rules = mkMerge (
          (flip map cfg.portForwards ({ to, port, protocol, ... }:
            let
              toPort = if to.port != null then to.port else port;
              toStr = "${to.host}:${toString toPort}";
            in
          ''
            ${protocol} dport ${toString (demangleIngress port)} \
              dnat ${toStr} \
              comment "External port ${toString port}"
          '')) ++

          (flip map cfg.portRangeForwards ({ to, portRange, protocol, ... }:
            let
              toStr =
                if to.fromPort != null && to.toPort != null && to.toPort - to.fromPort != portRange.to - portRange.from then
                  # mapping with different sized ranges => let underlying implementation handle it
                  "${to.host}:${toString to.fromPort}-${toString to.toPort}"
                else
                  # same size port mapping => map deterministically with literal map
                  let
                    dstBasePort = if to.fromPort != null then to.fromPort else portRange.from;
                    portMapping = concatMapStringsSep ", " (i: (
                      toString (demangleIngress (portRange.from + i)) + " : " + toString (dstBasePort + i)
                    )) (range 0 (portRange.to - portRange.from));
                  in "${to.host}:${protocol} dport map { ${portMapping} }";
             in
               ''
                  ${protocol} dport ${toString (demangleIngress portRange.from)}-${toString (demangleIngress portRange.to)} \
                    dnat ${toStr} \
                    comment "External port range ${toString portRange.from}:${toString portRange.to}"
                ''
          ))
        );
      };
    };
  };
}
