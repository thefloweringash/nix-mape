{ lib, ... }:

# A basic firewall layout.

let
  inherit (lib) mkMerge mkBefore mkAfter;

  statefulRules = mkMerge [
    (mkBefore ''
      ct state { established, related } accept
    '')
    (mkAfter ''
      jump log-refuse
    '')
  ];
in
{
  networking.mape.nftables = {
    tables.filter.chains.log-refuse = {
      rules = ''
        tcp flags & (fin | syn | rst | ack) == syn log prefix "refused connection: " counter
        jump refuse
      '';
    };

    tables.filter.chains.refuse = {
      rules = ''
        tcp flags & (fin | syn | rst | ack) != syn reject with tcp reset
        reject with icmp type port-unreachable
        reject with icmpv6 type port-unreachable
      '';
    };

    tables.filter.chains.input = {
      baseChain = {
        type = "filter";
        hook = "input";
        policy = "drop";
      };
      rules = mkMerge [
        statefulRules
        "iif lo accept"
        "ip6 daddr fe80::/64 udp dport 546 accept"
        "icmpv6 type { redirect, 139 } drop"
        "ip6 nexthdr icmpv6 accept"
      ];
    };

    tables.filter.chains.forward = {
      baseChain = {
        type = "filter";
        hook = "forward";
        policy = "drop";
      };

      rules = statefulRules;
    };

    tables.filter.chains.output = {
      baseChain = {
        type = "filter";
        hook = "output";
      };
    };

    tables.raw.chains.prerouting = {
      baseChain = {
        type = "filter";
        hook = "prerouting";
        priority = "raw";
      };
      rules = ''
        jump rpfilter
      '';
    };

    tables.raw.chains.rpfilter = {
      rules = ''
        fib saddr . mark . iif oif != 0 return
        meta nfproto ipv4 udp sport 67 udp dport 68 return
        meta nfproto ipv4 ip daddr 255.255.255.255 udp sport 68 udp dport 67 return
        log prefix "rpfilter refuse: " counter drop
      '';
    };
  };
}
