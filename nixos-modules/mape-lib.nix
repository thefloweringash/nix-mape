let
  inherit (builtins) bitAnd bitOr foldl';
in rec {
  bitOrList = xs: foldl' bitOr 0 xs;

  shr4 = x: x / 16;
  shl4 = x: x * 16;
  shr8 = x: x / 256;
  shl8 = x: x * 256;

  # virtual port -> real port
  mangleEgress = port:
    let low =  bitAnd port 15;    # 0x000f
        high = bitAnd port 240;   # 0x00f0
        cid =  bitAnd port 65280; # 0xff00
    in
      bitOrList [
        low
        (shr4 cid)
        (shl8 high)
      ];

  # real port -> virtual port
  demangleIngress = port:
    let low =  bitAnd port 15;    # 0x000f
        cid =  bitAnd port 4080;  # 0x0ff0
        high = bitAnd port 61440; # 0xf000
    in
      bitOrList [
        low
        (shl4 cid)
        (shr8 high)
      ];

}
