{ config, lib, pkgs, ... }:

# Spine of nftables composition. Without any rules or defaults.

let
  inherit (lib) mkOption types concatStrings mapAttrsToList optionalString mkIf
    concatStringsSep;

  cfg = config.networking.mape.nftables;

  tableType = {
    options = {
      family = mkOption {
        type = types.enum [ "ip" "ip6" "inet" "netdev" "arp" "bridge" ];
        default = "inet";
      };

      chains = mkOption {
        type = types.attrsOf (types.submodule chainType);
        default = {};
      };

      sets = mkOption {
        type = types.attrsOf (types.submodule setType);
        default = {};
      };
    };
  };

  baseChainType = {
    options = {
      type = mkOption {
        type = types.enum [ "filter" "route" "nat" ];
      };

      hook = mkOption {
        type = types.enum [ "prerouting" "input" "forward" "output" "postrouting" "ingress" ];
      };

      policy = mkOption {
        type = types.enum [ "accept" "drop" ];
        default = "accept";
      };

      priority = mkOption {
        type = types.either (types.enum ["raw" "filter" "mangle" "srcnat" "dstnat"]) types.int;
        default = 0;
      };
    };
  };

  chainType = {
    options = {
      # TODO: naming
      baseChain = mkOption {
        type = types.nullOr (types.submodule baseChainType);
        default = null;
      };

      rules = mkOption {
        type = types.lines;
        default = "";
      };
    };
  };

  renderAttrs = renderFunc: xs: concatStrings (mapAttrsToList renderFunc xs);

  renderTables = renderAttrs (name: table: ''
    table ${table.family} ${name} {
      ${renderSets table.sets}
      ${renderChains table.chains}
    }
  '');

  renderChains = renderAttrs (name: chain: ''
    chain ${name} {
      ${optionalString (chain.baseChain != null) (
        let inherit (chain.baseChain) type hook priority policy;
        in "type ${type} hook ${hook} priority ${toString priority}; policy ${policy};"
      )}

      ${chain.rules}
    }
  '');

  renderSets = renderAttrs (name: set: ''
    set ${name} {
      type ${set.type}; ${optionalString (set.flags != []) "flags ${concatStringsSep "," set.flags};"}
      ${optionalString (set.elements != []) ''
        elements = { ${(concatStringsSep "," set.elements)} }
      ''}
    }
  '');

  setType = {
    options = {
      type = mkOption {
        type = types.str;
      };

      flags = mkOption {
        type = types.listOf (types.enum ["constant" "interval" "timeout"]);

        default = [];
      };

      elements = mkOption {
        type = types.listOf types.str;

        default = [];
      };
    };
  };
in
{
  options = {
    networking.mape = {
      nftables.tables = mkOption {
        type = types.attrsOf (types.submodule tableType);

        default = {};
      };
    };
  };

  config = mkIf (cfg.tables != {}) {
    networking.firewall.enable = false;
    networking.nftables.enable = true;
    networking.nftables.ruleset = renderTables cfg.tables;
  };
}
